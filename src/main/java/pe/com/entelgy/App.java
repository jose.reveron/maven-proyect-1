package pe.com.entelgy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
	final static Logger logger = LogManager.getLogger(App.class);
    public static void main( String[] args )
    {
        
        logger.info("a test info message");
        logger.error("a test error message");
    }
}
